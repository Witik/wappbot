FROM node:18-slim as wapp-builder

RUN  apt-get update \
     && apt-get install -y wget gnupg ca-certificates musl \
     && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
     && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
     && apt-get update \
     # We install Chrome to get all the OS level dependencies, but Chrome itself
     # is not actually used as it's packaged in the node puppeteer library.
     # Alternatively, we could could include the entire dep list ourselves
     # (https://github.com/puppeteer/puppeteer/blob/master/docs/troubleshooting.md#chrome-headless-doesnt-launch-on-unix)
     # but that seems too easy to get out of date.
     && apt-get install -y google-chrome-stable \
     && rm -rf /var/lib/apt/lists/*

# When installing Puppeteer through npm, instruct it to not download Chromium.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

RUN mkdir /project
WORKDIR /project

# COPY the package.json and package-lock.json.
COPY package*.json ./

ARG NPM_CI_ARGS=--quiet
# install the npm dependencies before copying the whole codebase into the image
RUN npm ci $NPM_CI_ARGS

# After installing dependencies copy the whole codebase into the Container to not invalidate the cache before
COPY tsconfig.json ./
COPY src ./src

FROM wapp-builder as wapp-build
ENV CHROME_PATH /usr/bin/google-chrome-stable
RUN npm run build
ENTRYPOINT npm run start
