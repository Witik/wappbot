import { FilterQuery } from 'mongoose';
import { Chat as WappChat } from 'whatsapp-web.js';
import { Chat as TellyChat } from 'node-telegram-bot-api';
import { BoundChats } from './model/BoundChats';
import { BoundChatsModel, BoundChatsDocument } from './schemas/BoundChats';

export const upsert = async (message: BoundChats): Promise<BoundChats | null> => {
  const query: FilterQuery<BoundChatsDocument> = {
    whatsAppChatId: message.whatsAppChatId,
  };
  return BoundChatsModel.findOneAndUpdate(query, message, {
    upsert: true,
  });
};

export const findByTelegramChat = async (chat: TellyChat): Promise<BoundChats | null> => {
  const query: FilterQuery<BoundChatsDocument> = {
    telegramChatId: `${chat.id}`,
  };
  return BoundChatsModel.findOne(query);
};

export const findByWhatsAppChat = async (chat: WappChat): Promise<BoundChats | null> => {
  const query: FilterQuery<BoundChatsDocument> = {
    whatsAppChatId: chat.id._serialized,
  };
  return BoundChatsModel.findOne(query);
};

export const removeWhatsAppChat = async (chat: WappChat): Promise<BoundChats | null> => {
  const query: FilterQuery<BoundChatsDocument> = {
    whatsAppChatId: chat.id._serialized,
  };
  return BoundChatsModel.findOneAndDelete(query);
};
