declare module 'qrcode-terminal' {
  function generate(qr: string, options: unknown): void;
}
