import * as Wapp from 'whatsapp-web.js';
import * as TelegramBot from 'node-telegram-bot-api';
import { CallbackQuery, Message as TellyMessage } from 'node-telegram-bot-api';
import * as QRCode from 'qrcode-terminal';
import * as fs from 'fs';
import fetch from 'node-fetch';
import * as process from 'process';
import { dirSync, setGracefulCleanup } from 'tmp';
import mongoConnectionInitializer from './utils/mongoConnectionInitializer';
import { privateChat } from './utils/constants';
import { handleMessage } from './wappMessageHandler';
import { escapeMarkdown } from './utils/stringUtils';
import {
  findByTelegramMessage,
  findByWhatsAppMessage,
  upsert as upsertMessageService,
} from './crossMessageService';
import {
  findByTelegramChat,
  findByWhatsAppChat,
  removeWhatsAppChat,
  upsert as upsertBoundChats,
} from './boundChatsService';

setGracefulCleanup();
const SESSION_PATH = process.env.SESSION_PATH;
const SESSION_ID = process.env.SESSION_ID;
const CHROME_PATH = process.env.CHROME_PATH || undefined;
const TEMP_DIR = dirSync().name;

const handlePromise = <T extends unknown[]>(promise: (...props: T) => Promise<unknown>): (...props: T) => void => (
  (...props) => {
    promise(...props).catch(console.error);
  }
);

(() => {
  let wappMessageCache = '';
  const client = new Wapp.Client({
    authStrategy: new Wapp.LocalAuth({
      dataPath: SESSION_PATH,
      clientId: SESSION_ID,
    }),
    puppeteer: {
      args: ['--no-sandbox'],
      executablePath: CHROME_PATH,
    },
  });

  const bot = new TelegramBot(process.env.BOT_TOKEN || '', {
    polling: true,
  });

  const handleMsg = handleMessage(bot, client);
  const handleMessages = async (messages: Array<Wapp.Message>) => {
    for (const message of messages) {
      // eslint-disable-next-line no-await-in-loop
      await handleMsg(message);
    }
  };

  client.on('qr', (qr) => {
    QRCode.generate(qr, { small: true });
  });

  client.on('ready', handlePromise(async () => {
    await bot.sendMessage(privateChat, 'Wapp connected!');
    const chats = await client.getChats();
    const unreadChats = chats.filter((chat) => chat.unreadCount > 0)
      .map((chat) => chat.fetchMessages({ limit: chat.unreadCount }));

    const messagesPerChat = await Promise.all(unreadChats);

    await handleMessages(messagesPerChat.flat());
  }));

  client.on('message', handlePromise(handleMsg));

  client.on('disconnected', handlePromise(async (reason) => {
    await bot.sendMessage(privateChat, `disconnected: ${reason}`, {
      reply_markup: {
        inline_keyboard: [[{
          callback_data: 'reconnect',
          text: 'reconnect',
        }]],
      },
    });
  }));

  bot.on('callback_query', handlePromise(async (query: CallbackQuery) => {
    await Promise.all([mongoInit, wappInit]);
    if (query.data === 'reconnect') {
      wappInit = client.initialize();
    }
    await bot.answerCallbackQuery(query.id);
  }));

  const findChat = async (query: string): Promise<Wapp.Chat> => {
    if (query.endsWith('@g.us') || query.endsWith('@c.us')) {
      const chat = await client.getChatById(query);
      if (chat) {
        return chat;
      }
    }
    const chats = await client.getChats();
    return chats.find((chat) => chat.name && chat.name.trim().startsWith(query));
  };

  const findChats = async (startsWithName: string): Promise<Wapp.Chat[]> => {
    const chats = await client.getChats();
    return chats.filter((chat) => chat.name && chat.name.trim().startsWith(startsWithName));
  };

  const findContact = async (query: string): Promise<Wapp.Contact> => {
    if (query.endsWith('@g.us') || query.endsWith('@c.us')) {
      const contact = await client.getContactById(query);
      if (contact) {
        return contact;
      }
    }
    const contacts = await client.getContacts();
    return contacts.find((contact) => contact.name && contact.name.trim().startsWith(query));
  };

  bot.onText(/\/loadMessages( \S+)?/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chat = await getChatFromMessageOrParam(message, match[1]?.trim());
    if (chat) {
      const messages = await chat.fetchMessages({ limit: 10 });
      await handleMessages(messages);
      await bot.sendMessage(message.chat.id, `Messages for ${chat.name} loaded.`);
    }
  }));

  bot.onText(/\/reconnect/, handlePromise(async (message) => {
    if (message.from.id !== +privateChat) {
      return;
    }
    wappInit = client.initialize();
    await Promise.all([mongoInit, wappInit]);
  }));

  bot.onText(/\/status/, handlePromise(async (message) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const state = await client.getState();
    await bot.sendMessage(message.chat.id, state);
  }));

  bot.onText(/\/ping/, handlePromise(async (message) => {
    if (message.from.id !== +privateChat) {
      return;
    }
    await bot.sendMessage(message.chat.id, 'Pong!');
  }));

  bot.onText(/\/findContact (\S+)/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const contact = await findContact(match[1]);
    if (contact) {
      await bot.sendMessage(message.chat.id, `${contact.name} (${contact.id._serialized})`);
    } else {
      await bot.sendMessage(message.chat.id, 'Not found!');
    }
  }));

  bot.onText(/\/findChat (\S+)/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chat = await findChat(match[1]);
    if (chat) {
      await bot.sendMessage(message.chat.id, `${chat.name} (${chat.id._serialized})`);
    } else {
      await bot.sendMessage(message.chat.id, 'Not found!');
    }
  }));

  bot.onText(/\/findChats (\S+)/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chats = await findChats(match[1]);
    if (chats.length > 0) {
      await bot.sendMessage(message.chat.id,
        chats.reduce((list, chat) => `${list}\n*${escapeMarkdown(chat.name)}* _\\(${escapeMarkdown(chat.id._serialized)}\\)_`, ''),
        { parse_mode: 'MarkdownV2' });
    } else {
      await bot.sendMessage(message.chat.id, 'Not found!');
    }
  }));

  bot.onText(/\/topChats/, handlePromise(async (message) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chats = (await client.getChats()).slice(0, 10);
    if (chats.length > 0) {
      await bot.sendMessage(message.chat.id,
        chats.reduce((list, chat) => `${list}\n*${escapeMarkdown(chat.name)}* _\\(${escapeMarkdown(chat.id._serialized)}\\)_`, ''),
        { parse_mode: 'MarkdownV2' });
    } else {
      await bot.sendMessage(message.chat.id, 'No chats found!');
    }
  }));

  const getChatFromMessageOrParam = async (message: TellyMessage, param: string): Promise<Wapp.Chat> => {
    let chat: Wapp.Chat;

    if (param) {
      chat = await findChat(param);
    } else {
      if (message.chat.id === message.from.id) {
        await bot.sendMessage(message.chat.id, 'Needs a chat argument in private chat!');
        return null;
      }
      const boundChats = await findByTelegramChat(message.chat);
      if (!boundChats) {
        await bot.sendMessage(message.chat.id, 'Chat not bound!');
        return null;
      }
      chat = await client.getChatById(boundChats.whatsAppChatId);
    }
    if (!chat) {
      await bot.sendMessage(message.chat.id, 'Chat not found!');
      return null;
    }
    return chat;
  };

  bot.onText(/\/syncChat( \S+)?/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chat = await getChatFromMessageOrParam(message, match[1]?.trim());
    if (chat) {
      const boundChats = await findByWhatsAppChat(chat);
      if (!boundChats) {
        await removeWhatsAppChat(chat);
        await bot.sendMessage(message.chat.id, 'Chat not found, unbound!');
      } else {
        await Promise.all([
          setPhoto(chat, message),
          setTitle(chat, message),
          setDescription(chat, message),
        ]);
        await bot.sendMessage(message.chat.id, `${chat.name} synced`);
      }
    }
  }));

  bot.onText(/\/users( \S+)?/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chat = await getChatFromMessageOrParam(message, match[1]?.trim());
    if (chat) {
      if (!chat.isGroup) {
        await bot.sendMessage(message.chat.id, `${chat.name} is not a group!`);
        return;
      }
      // @ts-ignore
      const userIds: string[] = chat.participants.map((p) => p.id._serialized);
      const users: Wapp.Contact[] = await Promise.all(
        userIds.map((id) => client.getContactById(id)),
      );

      await bot.sendMessage(message.chat.id, `*${escapeMarkdown(chat.name)}* users:${
        users.filter((user) => !user.isMe)
          .reduce((list, user) => `${list}\n${escapeMarkdown(user.name || user.pushname)} _\\(\\+${user.number}\\)_`, '')
      }`, { parse_mode: 'MarkdownV2' });
    }
  }));

  bot.onText(/\/description( \S+)?/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chat = await getChatFromMessageOrParam(message, match[1]?.trim());
    if (chat) {
      if (!chat.isGroup) {
        await bot.sendMessage(message.chat.id, `${chat.name} is not a group!`);
        return;
      }

      // @ts-ignore
      const { description } = chat;
      await bot.sendMessage(message.chat.id,
        `*${escapeMarkdown(chat.name)}* description:\n${escapeMarkdown(description as string)}`,
        { parse_mode: 'MarkdownV2' });
    }
  }));

  async function setPhoto(chat: Wapp.Chat, message: TelegramBot.Message) {
    const pic = await client.getProfilePicUrl(chat.id._serialized);
    if (pic) {
      const response = await fetch(pic);
      if (response.ok) {
        await bot.setChatPhoto(message.chat.id, (await response.buffer()));
      }
    }
  }

  async function setTitle(chat: Wapp.Chat, message: TelegramBot.Message) {
    if (chat.isGroup) {
      await bot.setChatTitle(message.chat.id, chat.name);
    } else {
      await bot.setChatTitle(message.chat.id, `${chat.name} Wapp`);
    }
  }

  async function setDescription(chat: Wapp.Chat, message: TelegramBot.Message) {
    if (chat.isGroup) {
      // @ts-ignore
      const { description } = chat;
      await bot.setChatDescription(message.chat.id, description as string ?? '');
    } else {
      const contact = await chat.getContact();
      if (contact != null) {
        const about = await contact.getAbout();
        await bot.setChatDescription(message.chat.id, about ?? '');
      }
    }
  }

  bot.onText(/\/bind( \S+)/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    if (message.chat.id === message.from.id) {
      await bot.sendMessage(message.chat.id, 'Cannot bind to private chat');
      return;
    }
    const chat = await getChatFromMessageOrParam(message, match[1]?.trim());
    if (chat) {
      await upsertBoundChats({
        telegramChatId: `${message.chat.id}`,
        whatsAppChatId: chat.id._serialized,
      });
      await Promise.all([
        setPhoto(chat, message),
        setTitle(chat, message),
        setDescription(chat, message),
        chat.fetchMessages({ limit: 10 }).then(handleMessages),
      ]);
      await bot.sendMessage(message.chat.id, `${chat.name} (${chat.id._serialized}) bound`);
    }
  }));

  bot.onText(/\/unbind (\S+)/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chat = await findChat(match[1]);
    if (!chat) {
      await bot.sendMessage(message.chat.id, 'Chat not found!');
    } else {
      await removeWhatsAppChat(chat);
      await bot.sendMessage(message.chat.id, `${chat.name} (${chat.id._serialized}) unbound`);
    }
  }));

  bot.onText(/\/send (\S+) (.+)/, handlePromise(async (message, match) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }
    const chat = await findChat(match[1]);
    if (chat) {
      await chat.sendMessage(match[2]);
    } else {
      await bot.sendMessage(message.chat.id, 'Chat not found!');
    }
  }));

  const sendMedia = async (messageId: string, fileId: string, mimeType: string, options?: Wapp.MessageSendOptions): Promise<Wapp.Message | undefined> => {
    let tempFile: string;
    try {
      tempFile = await bot.downloadFile(fileId, TEMP_DIR);
    } catch (e) {
      console.error(`Error getting telegram media (mime: ${mimeType} id: ${fileId})`);
      console.error(e);
      if (fs.existsSync(tempFile)) {
        fs.unlinkSync(tempFile);
      }
      return undefined;
    }

    if (tempFile && fs.existsSync(tempFile)) {
      try {
        const media = Wapp.MessageMedia.fromFilePath(tempFile);
        if (media.mimetype) {
          console.log('mimetype inferred as', media.mimetype);
        } else {
          media.mimetype = mimeType;
        }
        return await client.sendMessage(messageId, media, options);
      } catch (e) {
        console.error(`Error sending telegram media to wapp (mime: ${mimeType} id: ${fileId})`);
        console.error(e);
      } finally {
        fs.unlinkSync(tempFile);
      }
    }
    return undefined;
  };

  bot.on('message', handlePromise(async (message) => {
    await Promise.all([mongoInit, wappInit]);
    if (message.from.id !== +privateChat) {
      return;
    }

    if (message.text && message.text.startsWith('/')) {
      return;
    }

    let whatsAppChatId: string;
    let whatsAppMessageId: string;
    if (message.chat.id === +privateChat) {
      if (message.reply_to_message) {
        const crossMessage = await findByTelegramMessage(message.reply_to_message);
        whatsAppChatId = crossMessage && crossMessage.whatsAppChatId;
      }
    } else {
      const boundChats = await findByTelegramChat(message.chat);
      whatsAppChatId = boundChats && boundChats.whatsAppChatId;
      if (message.reply_to_message) {
        const crossMessage = await findByTelegramMessage(message.reply_to_message);
        whatsAppMessageId = crossMessage && crossMessage.whatsAppMessageId;
      }
    }

    if (!whatsAppChatId) {
      return;
    }

    let sentMessage: Wapp.Message;
    if (message.text) {
      sentMessage = await client.sendMessage(whatsAppChatId, message.text, {
        quotedMessageId: whatsAppMessageId,
      });
    } else if (message.photo) {
      const fileId = message.photo[message.photo.length - 1].file_id;
      const fileLink = await bot.getFileLink(fileId);
      const response = await fetch(fileLink);

      if (response.ok) {
        const media = new Wapp.MessageMedia('image/png', (await response.buffer()).toString('base64'));
        sentMessage = await client.sendMessage(whatsAppChatId, media, {
          caption: message.caption,
        });
      }
    } else if (message.sticker) {
      const fileId = message.sticker.is_animated ? message.sticker.thumb.file_id : message.sticker.file_id;
      const fileLink = await bot.getFileLink(fileId);
      const response = await fetch(fileLink);

      if (response.ok) {
        const media = new Wapp.MessageMedia('image/webp', (await response.buffer()).toString('base64'));
        sentMessage = await client.sendMessage(whatsAppChatId, media, {
          sendMediaAsSticker: true,
        });
      }
    } else if (message.animation) {
      const fileId = message.animation.file_id;
      const mimeType = message.animation.mime_type || 'video/mp4';
      const fileLink = await bot.getFileLink(fileId);
      const response = await fetch(fileLink);

      if (response.ok) {
        const media = new Wapp.MessageMedia(mimeType, (await response.buffer()).toString('base64'));
        sentMessage = await client.sendMessage(whatsAppChatId, media, {
          sendMediaAsSticker: true,
        });
      }
    }
    /*
    else if (message.photo) {
    const fileId = message.photo[message.photo.length - 1].file_id;
    sentMessage = await sendMedia(whatsAppMessageId, fileId, 'image/png', {
      caption: message.caption,
    });
  } else if (message.sticker) {
    const fileId = message.sticker.is_animated ? message.sticker.thumb.file_id : message.sticker.file_id;
    sentMessage = await sendMedia(whatsAppMessageId, fileId, 'image/webp', {
      sendMediaAsSticker: true,
    });
  } else if (message.animation) {
    const fileId = message.animation.file_id;
    const mimeType = message.animation.mime_type || 'video/mp4';
    sentMessage = await sendMedia(whatsAppMessageId, fileId, mimeType);
  }
  */

    if (sentMessage) {
      wappMessageCache = sentMessage.id._serialized;

      const checkMark = await bot.sendMessage(message.chat.id, '✅');

      await upsertMessageService({
        telegramChatId: `${message.chat.id}`,
        telegramMessageIds: [`${message.message_id}`, `${checkMark.message_id}`],
        whatsAppMessageId: sentMessage.id._serialized,
        whatsAppChatId,
        whatsAppUserId: sentMessage.author || sentMessage.from,
      });
    }
  }));

  client.on('message_create', handlePromise(async (msg) => {
    if (msg.fromMe) {
      const crossMessage = await findByWhatsAppMessage(msg);
      if (!crossMessage && wappMessageCache !== msg.id._serialized) {
        await handleMsg(msg);
      }
    }
  }));

  client.on('group_join', handlePromise(async (notification) => {
    let telegramMessage = '';

    const from = await notification.getContact();
    telegramMessage += `*${escapeMarkdown(from.name || from.pushname || from.shortName)}*`;

    const chat = await notification.getChat();
    telegramMessage += `joined _${escapeMarkdown(chat.name)}_`;

    telegramMessage += `\n${escapeMarkdown(notification.body)}`;

    await bot.sendMessage(privateChat, telegramMessage, {
      parse_mode: 'MarkdownV2',
    });
  }));

  client.on('group_leave', handlePromise(async (notification) => {
    let telegramMessage = '';

    const from = await notification.getContact();
    telegramMessage += `*${escapeMarkdown(from.name || from.pushname || from.shortName)}*`;

    const chat = await notification.getChat();
    telegramMessage += `left _${escapeMarkdown(chat.name)}_`;

    telegramMessage += `\n${escapeMarkdown(notification.body)}`;

    await bot.sendMessage(privateChat, telegramMessage, {
      parse_mode: 'MarkdownV2',
    });
  }));

  client.on('group_update', handlePromise(async (notification) => {
    let telegramMessage = '';

    const from = await notification.getContact();
    telegramMessage += `*${escapeMarkdown(from.name || from.pushname || from.shortName)}*`;

    const chat = await notification.getChat();
    telegramMessage += `in _${escapeMarkdown(chat.name)}_`;

    telegramMessage += `\n${escapeMarkdown(notification.body)}`;

    await bot.sendMessage(privateChat, telegramMessage, {
      parse_mode: 'MarkdownV2',
    });
  }));

  const mongoInit = mongoConnectionInitializer();
  let wappInit = client.initialize();
})();
