export interface BoundChats {
  whatsAppChatId: string,
  telegramChatId: string,
}
