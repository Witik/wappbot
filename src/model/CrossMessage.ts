export interface CrossMessage {
  whatsAppMessageId: string,
  whatsAppChatId: string,
  whatsAppUserId: string,
  telegramMessageIds: string[],
  telegramChatId: string,
}
