import { connect, ConnectOptions } from 'mongoose';
import * as process from 'process';

export default async (): Promise<void> => {
  let options: ConnectOptions = {
  };

  if (process.env.MONGODB_USERNAME !== undefined) {
    options.auth = {
      username: process.env.MONGODB_USERNAME,
      password: process.env.MONGODB_PASSWORD,
    };
  }
  const mongoUrl = process.env.MONGODB_URI || 'mongodb://localhost:27017/whatsapp-bot';

  await connect(mongoUrl, options);
};
