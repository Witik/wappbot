export const escapeMarkdown = (body: string | undefined): string => (
  body?.replace(/[_*[\]()~`>#+\-=|{}.!]/g, '\\$&') ?? ''
);
