import { Document, model, Schema } from 'mongoose';
import { BoundChats } from '../model/BoundChats';

export interface BoundChatsDocument extends BoundChats, Document {
}

const boundChats = new Schema({
  whatsAppChatId: String,
  telegramChatId: String,
});

export const BoundChatsModel = model<BoundChatsDocument>('boundChats', boundChats);
