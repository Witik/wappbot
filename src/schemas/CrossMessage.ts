import { Document, model, Schema } from 'mongoose';
import { CrossMessage } from '../model/CrossMessage';

export interface CrossMessageDocument extends CrossMessage, Document {
}

const crossMessage = new Schema({
  whatsAppMessageId: String,
  whatsAppChatId: String,
  whatsAppUserId: String,
  telegramMessageIds: [String],
  telegramChatId: String,
});

export const CrossMessageModel = model<CrossMessageDocument>('crossMessage', crossMessage);
