import TelegramBot, { Message as TelegramMessage } from 'node-telegram-bot-api';
import {
  Chat as WappChat,
  Client as WappClient,
  Message as WappMessage, MessageMedia,
  MessageTypes as WappMessageTypes,
} from 'whatsapp-web.js';

import * as sharp from 'sharp';
import * as VCard from 'vcf';

import { escapeMarkdown } from './utils/stringUtils';
import { findByWhatsAppMessage, upsert } from './crossMessageService';
import { findByWhatsAppChat } from './boundChatsService';
import { privateChat } from './utils/constants';

const getFrom = async (message: WappMessage): Promise<string> => {
  if (message.fromMe) {
    return '*You*';
  }
  const from = await message.getContact();
  return `*${escapeMarkdown(from.name || from.pushname || from.shortName)}*`;
};

const getDest = (message: WappMessage, chat: WappChat): string => {
  if (chat.isGroup) {
    return `in _${escapeMarkdown(chat.name)}_`;
  }
  if (message.fromMe) {
    return `to _${escapeMarkdown(chat.name)}_`;
  }
  return '_direct_';
};

const formatDate = (messageDate: Date): string => (
  `${messageDate.getDate()}-${messageDate.getMonth() + 1}-${messageDate.getFullYear()}`
);

const leadingZero = (num: number): string => (num < 10 ? `0${num}` : `${num}`);

const formatTime = (messageDate: Date): string => (
  `${leadingZero(messageDate.getHours())}:${leadingZero(messageDate.getMinutes())}:${leadingZero(messageDate.getSeconds())}`
);

const getPrefix = async (message: WappMessage, chat: WappChat, bound: boolean): Promise<string> => {
  let telegramMessage = await getFrom(message);

  if (!bound) {
    telegramMessage += ` ${getDest(message, chat)}`;
  }

  const now = new Date();
  const messageDate = new Date(message.timestamp * 1000);
  if (now.getTime() - messageDate.getTime() > 600000) {
    telegramMessage += ` ${escapeMarkdown(formatTime(messageDate))}`;
    const formattedDate = formatDate(messageDate);
    if (formatDate(now) !== formattedDate) {
      telegramMessage += ` ${escapeMarkdown(formattedDate)}`;
    }
  }

  return `${telegramMessage}\n`;
};

export const handleMessage = (bot: TelegramBot, client: WappClient): ((message: WappMessage) => Promise<void>) => {
  const wappMessageToTelegram = async (message: WappMessage): Promise<TelegramMessage[]> => {
    const chat = await message.getChat();
    const boundChats = await findByWhatsAppChat(chat);
    const telegramChat = boundChats ? boundChats.telegramChatId : privateChat;
    let messageText = await getPrefix(message, chat, boundChats != null);

    let replyToMessageId: number;
    if (message.hasQuotedMsg) {
      const quotedMessage = await message.getQuotedMessage();
      const crossMessage = await findByWhatsAppMessage(quotedMessage);
      if (crossMessage && crossMessage.telegramChatId === telegramChat) {
        // TODO: if the quoted message is not sent by me, quote the first, otherwise, the last.
        replyToMessageId = +crossMessage.telegramMessageIds[0];
      }
    }

    const escapedBody = escapeMarkdown(message.body);
    switch (message.type) {
      case WappMessageTypes.STICKER: {
        let media: MessageMedia;
        try {
          media = await message.downloadMedia();
          messageText += 'Sent sticker:';
        } catch (e) {
          messageText += `Sent sticker but retrieving failed:\n${(e as Error).message}`;
        }
        const telegramMessage = await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        });
        if (media) {
          const sticker = await sharp(Buffer.from(media.data, 'base64')).webp({}).toBuffer();
          return [
            telegramMessage,
            await bot.sendSticker(telegramChat, sticker),
          ];
        }
        return [telegramMessage];
      }
      case WappMessageTypes.CONTACT_CARD: {
        messageText += 'Sent contact:';
        const card = new VCard().parse(message.body.replace(/([^\r])\n/g, '$1\r\n'));
        return [
          await bot.sendMessage(telegramChat, messageText, {
            parse_mode: 'MarkdownV2',
            reply_to_message_id: replyToMessageId,
          }),
          await bot.sendContact(telegramChat, card.get('tel').valueOf().toString(), card.get('fn').valueOf().toString(), {
            vcard: message.body,
          }),
        ];
      }
      case WappMessageTypes.CONTACT_CARD_MULTI: {
        messageText += 'Sent multiple contacts:';
        const tellyMessages = [await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        })];
        const cards: any[] = VCard.parse(message.body.replace(/([^\r])\n/g, '$1\r\n'));
        const cardMessages = await Promise.all(
          cards.map((card) => bot.sendContact(telegramChat, card.get('tel').valueOf(), card.get('fn').valueOf(), {
            vcard: message.body,
          })),
        );
        return tellyMessages.concat(cardMessages);
      }
      case WappMessageTypes.LOCATION: {
        messageText += 'Sent Location:';
        return [
          await bot.sendMessage(telegramChat, messageText, {
            parse_mode: 'MarkdownV2',
            reply_to_message_id: replyToMessageId,
          }),
          await bot.sendLocation(telegramChat, +message.location.latitude, +message.location.longitude),
        ];
      }
      case WappMessageTypes.AUDIO: {
        let media: MessageMedia;
        try {
          media = await message.downloadMedia();
          messageText += 'Sent audio:';
        } catch (e) {
          messageText += `Sent audio but retrieving failed:\n${(e as Error).message}`;
        }
        const telegramMessage = await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        });
        if (media) {
          return [
            telegramMessage,
            await bot.sendAudio(telegramChat, Buffer.from(media.data, 'base64'), {
              caption: message.body,
            },
            // @ts-ignore
            {
              filename: media.filename,
              contentType: media.mimetype,
            }),
          ];
        }
        return [telegramMessage];
      }
      case WappMessageTypes.VOICE: {
        let media: MessageMedia;
        try {
          media = await message.downloadMedia();
          messageText += 'Sent voice:';
        } catch (e) {
          messageText += `Sent voice but retrieving failed:\n${(e as Error).message}`;
        }
        const telegramMessage = await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        });
        if (media) {
          return [
            telegramMessage,
            await bot.sendVoice(telegramChat, Buffer.from(media.data, 'base64'), {
              caption: message.body,
            },
            // @ts-ignore
            {
              filename: media.filename,
              contentType: media.mimetype,
            }),
          ];
        }
        return [telegramMessage];
      }
      case WappMessageTypes.IMAGE: { // TODO: album?
        let media: MessageMedia;
        try {
          media = await message.downloadMedia();
          messageText += 'Sent image:';
        } catch (e) {
          messageText += `Sent image but retrieving failed:\n${(e as Error).message}`;
        }
        const telegramMessage = await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        });
        if (media) {
          return [
            telegramMessage,
            await bot.sendPhoto(telegramChat, Buffer.from(media.data, 'base64'), {
              caption: message.body,
            },
            // @ts-ignore
            {
              filename: media.filename,
              contentType: media.mimetype,
            }),
          ];
        }
        return [telegramMessage];
      }
      case WappMessageTypes.VIDEO: { // TODO: can also be a GIF, sendAnimation?
        let media: MessageMedia;
        try {
          media = await message.downloadMedia();
          messageText += 'Sent video:';
        } catch (e) {
          messageText += `Sent video but retrieving failed:\n${(e as Error).message}`;
        }
        const telegramMessage = await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        });
        if (media) {
          return [
            telegramMessage,
            await bot.sendVideo(telegramChat, Buffer.from(media.data, 'base64'), {
              caption: message.body,
            },
            // @ts-ignore
            {
              filename: media.filename,
              contentType: media.mimetype,
            }),
          ];
        }
        return [telegramMessage];
      }
      case WappMessageTypes.DOCUMENT: {
        let media: MessageMedia;
        try {
          media = await message.downloadMedia();
          messageText += 'Sent document:';
        } catch (e) {
          messageText += `Sent document but retrieving failed:\n${(e as Error).message}`;
        }
        const telegramMessage = await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        });
        if (media) {
          return [
            telegramMessage,
            await bot.sendDocument(telegramChat, Buffer.from(media.data, 'base64'), {
              caption: message.body,
            },
            {
              filename: media.filename,
              contentType: media.mimetype,
            }),
          ];
        }
        return [telegramMessage];
      }
      default: {
        if (message.hasMedia) {
          let media: MessageMedia;
          try {
            media = await message.downloadMedia();
            messageText += 'Sent media:';
          } catch (e) {
            messageText += `Sent media but retrieving failed:\n${(e as Error).message}`;
          }
          const telegramMessage = await bot.sendMessage(telegramChat, messageText, {
            parse_mode: 'MarkdownV2',
            reply_to_message_id: replyToMessageId,
          });
          if (media) {
            return [
              telegramMessage,
              await bot.sendDocument(telegramChat, Buffer.from(media.data, 'base64'), {
                caption: message.body,
              },
              {
                filename: media.filename,
                contentType: media.mimetype,
              }),
            ];
          }
          return [telegramMessage];
        }
        messageText += escapedBody;
        return [await bot.sendMessage(telegramChat, messageText, {
          parse_mode: 'MarkdownV2',
          reply_to_message_id: replyToMessageId,
        })];
      }
    }
  };

  return async (message: WappMessage) => {
    const tellyMessages = await wappMessageToTelegram(message);
    const wappChatId = message.fromMe ? message.to : message.from;
    await upsert({
      whatsAppMessageId: message.id._serialized,
      whatsAppChatId: wappChatId,
      whatsAppUserId: message.author || message.from,
      telegramChatId: `${tellyMessages[0].chat.id}`,
      telegramMessageIds: tellyMessages.map((tMessage) => `${tMessage.message_id}`),
    });
    await client.sendSeen(wappChatId);
  };
};
