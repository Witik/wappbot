import { FilterQuery } from 'mongoose';
import { Message as WappMessage } from 'whatsapp-web.js';
import { Message as TellyMessage } from 'node-telegram-bot-api';
import { CrossMessage } from './model/CrossMessage';
import { CrossMessageModel, CrossMessageDocument } from './schemas/CrossMessage';

export const upsert = async (message: CrossMessage): Promise<CrossMessage> => {
  const query: FilterQuery<CrossMessageDocument> = {
    whatsAppChatId: message.whatsAppChatId,
    whatsAppMessageId: message.whatsAppMessageId,
  };
  return CrossMessageModel.findOneAndUpdate(query, message, {
    upsert: true,
  });
};

export const findByTelegramMessage = async (message: TellyMessage): Promise<CrossMessage> => {
  const query: FilterQuery<CrossMessageDocument> = {
    telegramChatId: `${message.chat.id}`,
    telegramMessageIds: `${message.message_id}`,
  };
  return CrossMessageModel.findOne(query);
};

export const findByWhatsAppMessage = async (message: WappMessage): Promise<CrossMessage> => {
  const query: FilterQuery<CrossMessageDocument> = {
    whatsAppChatId: message.fromMe ? message.to : message.from,
    whatsAppMessageId: message.id._serialized,
  };
  return CrossMessageModel.findOne(query);
};
