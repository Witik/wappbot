#!/usr/bin/env sh

sshport=$SSH_PORT
sshlogin=$SSH_LOGIN
hostname=$SSH_HOST

# shellcheck disable=SC2088
deploydir="~/deploy/wappbot-docker/"

filename="whatsapp-bot.tar"

echo "deleting $deploydir"
ssh -p "$sshport" "$sshlogin@$hostname" "rm -rf $deploydir*"
echo 'deleted old deploy'

scp -P "$sshport" "./$filename" "$sshlogin@$hostname":$deploydir

ssh -p "$sshport" "$sshlogin@$hostname" "docker load < $deploydir$filename"
ssh -p "$sshport" "$sshlogin@$hostname" "cd deploy && ./restartWappDocker.sh"

echo "Deploy finished!"
